import express from 'express'
import cors from 'cors'
import passport from 'passport'
import 'dotenv/config'

import { connect } from './src/sequelize/index.js'
import router from './src/routes/index.js'
import { genericRequestLogger } from './src/middleware/genericRequestLogger.js'

const PORT = process.env.PORT || 8081
const corsOptions = {
    origin: `http://localhost:${PORT}`
}

const app = express()
connect()

app.use(express.json())
app.use(passport.initialize())
app.use(cors(corsOptions))
app.use(genericRequestLogger)

const setJSON = ( _, res, next) => {
    res.setHeader('Content-Type','application/json')
    next()
}

app.use('/', setJSON, router)

// app.get('/getusers/:id', ( req, res) => {
//     console.log('Iddddddd:',req.params.id)
//     res.send({
//         message: "Request received!"
//     })
// })
// let a = new Buffer.from('root:enigma@123').toString('base64')
// console.log(`Console Basic ${a}`)

app.listen(PORT, () => {
    console.log(`Listening at PORT ${PORT}, kindly use the link http://localhost:${PORT}/`)
})