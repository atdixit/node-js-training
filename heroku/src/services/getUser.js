import { User } from "../models/User.js"

export const getUserById = async (id) => {
    const user = await User.findOne({ 
        where: { 
            id 
        },
        attributes: {
            exclude: ['password']
        }
    })
    return user
}

export const getUsers = async () => {
    const users = await User.findAll({
        attributes: {
            exclude: [ 'password']
        },
        order: [['id','ASC']]
    })
    return users
}

export const getUserIdPassword = async (id) => {
    const user = await User.findOne({
        where: {
            id
        },
        attributes: {
            include: [ 'id', 'password', 'name' ]
        }
    })
    return user
}