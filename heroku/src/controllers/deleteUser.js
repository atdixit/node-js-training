import { deleteUserById, deleteAllUsers } from '../services/deleteUser.js'
import { getUsers } from '../services/getUser.js'

export const handleDeleteUser = async ( req, res ) => {
    if(req.params.id){
        const result = await deleteUserById(req.params.id)
        if(result){
            res.send({
                message: `Deleted user with id: ${req.params.id}`,
                data: result
            })
        }else{
            res.send({ 
                message: 'Not found user with given id'
            })
        }
    }else{
        res.status(500).send({ 
            error: 'Kindly enter a valid id'
        })
    }
}

export const handleDeleteAllUser = async ( req , res ) => {
    await deleteAllUsers()
    const result = await getUsers()
    if(result && result.length === 0){
        res.send({
            message: "Deleted all users",
        })
    }else{
        res.send({ 
            error: 'Table Empty!'
        })
    }
}