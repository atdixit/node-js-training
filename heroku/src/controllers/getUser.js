import { getUserById, getUsers } from "../services/getUser.js"

export const handleGetUser = async ( req, res ) => {
    if(req.params.id){
        const result = await getUserById(req.params.id)
        if(result){
            res.send({
                message: `Found one entry with id: ${req.params.id}`,
                data: result
            })
        }else{
            res.send({ 
                message: 'Not found user with given id'
            })
        }
    }else{
        res.status(500).send({ 
            error: 'Kindly enter a valid id'
        })
    }
}

export const handleGetAllUsers = async ( req , res ) => {
    const result = await getUsers()
    if(result && result.length > 0){
        res.send({
            message: "Found entries",
            data: result
        })
    }else{
        res.send({ 
            error: 'Table Empty!'
        })
    }
}