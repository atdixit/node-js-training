import express from 'express'
import bodyParser from 'body-parser'
import { auth, logger } from './middleware.js'

const PORT = 8080

const app = express()

app.use(bodyParser.json())
app.use(logger)

app.get('/auth-data', auth)

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})