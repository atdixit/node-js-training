import { ExtractJwt, Strategy } from 'passport-jwt'
import { getUserById } from '../services/getUser.js';

export const passportUsage = (passport) => passport.use(new Strategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET_KEY,
    passReqToCallback: true
}, async ( req, jwt_payload, next) => {
    const user = await getUserById(jwt_payload.id)
    // console.log('UserPassport',user)
    if(user){
        req.user=user
        return next(null, user)
    }else{
        return next(null, false)
    }
}))