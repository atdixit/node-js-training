import { User } from "../models/User.js"

export const insertUser = async ( insert_data ) => {
    const result = await User.create({
        ...insert_data,
    })
    return result
}