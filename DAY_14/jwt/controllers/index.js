import { handleInsertUser } from "./insertUser.js"
import { handleGetUser, handleGetAllUsers } from "./getUser.js"
import { handleDeleteUser, handleDeleteAllUser } from "./deleteUser.js"
import { handleLogin } from './loginUser.js'
import { handleUpdateUserById } from "./updateUser.js"

export {
    handleInsertUser,
    handleGetUser,
    handleGetAllUsers,
    handleDeleteAllUser,
    handleDeleteUser,
    handleLogin,
    handleUpdateUserById
}