import { DataTypes, Sequelize } from 'sequelize'
import User from "./User.js"
import Order from "./Order.js"

const { DB_HOST, DB_USER, DB_NAME, DB_PASS } = process.env 
// || {
//     DB_HOST: 'localhost',
//     DB_USER: 'postgres',
//     DB_NAME: 'usertest',
//     DB_PASS: 'root'
// }

export const sequelize = new Sequelize(DB_NAME,DB_USER,DB_PASS,{
    logging: console.log,
    host: DB_HOST,
    dialect: 'postgres'
})
sequelize.authenticate()
.then(() => {
    console.log('Connection Successful!')
})
.catch((err) => {
    console.log(`Unable to connect : ${err}`)
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.users = User(sequelize, DataTypes)
db.orders = Order(sequelize, DataTypes)

db.sequelize.sync()
.then(() => {
    console.log('Sync Successful!')
})
.catch(() => {
    console.log('Unable to Sync!')
})

db.users.hasMany(db.orders,{
    as: "orders",
    foreignKey: "user_id",
    onDelete: "cascade"
})
db.orders.belongsTo(db.users, { 
    as: "users",
    foreignKey: "user_id",
    onDelete: "cascade"
})

export default db