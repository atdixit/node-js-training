export default (sequelize, DataTypes) => {
    const User = sequelize.define('users', {
        user_id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type:DataTypes.STRING,
            allowNull: false
        }
    }, {
        tableName: "users",
        timestamps: false
    })
    return User
}
