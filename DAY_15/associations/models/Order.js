export default ( sequelize, DataTypes ) => {
    const Order = sequelize.define('orders', {
        order_id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        item_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        qty: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        user_id: {
            type: DataTypes.UUID
        }
    }, {
        tableName: "orders",
    })
    return Order
}
