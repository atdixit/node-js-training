import db from "../models/index.js"

const { users } = db

export const deleteUserById = async ( user_id ) => {
    const result = await users.destroy({
        where: {
            user_id
        },
        returning:true,
        restartIdentity: true
    })
    return result
}
