import db from "../models/index.js"

const { users } = db

export const getUserById = async ( user_id ) => {
    // console.log('undefinedValue In getUser.js', user_id)
    const user = await users.findOne({ 
        where: { 
            user_id
        },
    })
    return user
}