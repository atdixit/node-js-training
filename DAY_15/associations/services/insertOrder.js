import db from "../models/index.js"

const { orders } = db

export const insertOrder = async ( insert_data ) => {
    const result = await orders.create({
        ...insert_data,
    })
    return result
}