import db from "../models/index.js"

const { orders, users } = db

export const getOrdersOfUserId = async ( user_id ) => {
    // console.log('undefinedValue',user_id)
    try {
        const result = await users.findAll({ 
            where: { 
                user_id,
            },
            include: [
                {
                    model: orders,
                    as: 'orders',
                    attributes: ['order_id', 'item_name', 'qty']
                }
            ]
        })
        return result        
    } catch (error) {
        console.log('Error here : '+ error)        
    }

}