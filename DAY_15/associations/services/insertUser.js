import db from "../models/index.js"

const { users } = db

export const insertUser = async ( insert_data ) => {
    const result = await users.create({
        ...insert_data,
    })
    return result
}