import db from "../models/index.js"

const { orders } = db

export const deleteOrderById = async ( order_id ) => {
    const result = await orders.destroy({
        where: {
            order_id
        },
        returning:true,
        restartIdentity: true
    })
    return result
}
