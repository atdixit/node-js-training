import express from 'express'
import passport from 'passport'
import { handleDeleteOrder } from '../controllers/deleteOrder.js'
import { handleDeleteUser } from '../controllers/deleteUser.js'
import { handleGetOrdersByUserId } from '../controllers/getOrder.js'
import { handleInsertOrder } from '../controllers/insertOrder.js'
import { handleInsertUser } from '../controllers/insertUser.js'
import { handleLogin } from '../controllers/loginUser.js'
import { passportUsage } from '../utils/passport.js'

passportUsage(passport)
const router = express.Router()

router.get('/user/:user_id/orders',passport.authenticate('jwt',{ session: false }),handleGetOrdersByUserId)
router.post('/insert-user',handleInsertUser)
router.post('/login-user',handleLogin)
router.post('/insert-order-by-user-id',passport.authenticate('jwt',{ session: false }), handleInsertOrder)
router.delete('/delete-user/:id',passport.authenticate('jwt',{ session: false }), handleDeleteUser)
router.delete('/delete-order/:id',passport.authenticate('jwt',{ session: false }),handleDeleteOrder)

export default router