import { getUserById } from "../services/getUser.js"
import jwt from 'jsonwebtoken'
import { generateSecurePassword } from "../utils/securedPassword.js"

export const handleLogin = async ( req, res ) => {
    if(req._body){
        if(req.body.user_id === process.env.ADMIN_ID && req.body.password === process.env.ADMIN_PASS){
            const token = jwt.sign({
                user_id: 'admin',
                name: 'admin'
            }, process.env.JWT_SECRET_KEY,{
                expiresIn: '18000s'
            })
            res.status(201).send({
                message: 'Admin token created!',
                token
            })
            return
        }
        if(req.body.user_id && req.body.password){
            const result = await getUserById(req.body.user_id)
            if(result && result.password === generateSecurePassword(req.body.password)){
                const jwt_payload = {
                    user_id: result.user_id,
                    name: result.name
                }
                const token = jwt.sign(jwt_payload, process.env.JWT_SECRET_KEY,{
                    expiresIn: '18000s'
                })
                // console.log('Token',token)
                res.status(201).send({
                    message: 'Token successfully created',
                    token
                })
            }
            else{
                res.status(403).send({
                    error:'Enter valid id and password'
                })
            }
        }else{
            res.status(403).send({
                error: 'Enter valid id and password'
            })
        }
    }else{
        res.status(403).send({
            error: 'Enter a valid body'
        })
    }
}
