import { deleteUserById} from '../services/deleteUser.js'

export const handleDeleteUser = async ( req, res ) => {
    if(req.params.id){
        const result = await deleteUserById(req.params.id)
        if(result){
            res.send({
                message: `Deleted user with id: ${req.params.id}`,
                data: result
            })
        }else{
            res.send({ 
                message: 'Not found user with given id'
            })
        }
    }else{
        res.status(500).send({ 
            error: 'Kindly enter a valid id'
        })
    }
}