import { deleteOrderById } from "../services/deleteOrder.js"

export const handleDeleteOrder = async ( req, res ) => {
    if(req.params.id){
        const result = await deleteOrderById(req.params.id)
        if(result){
            res.send({
                message: `Deleted order with id: ${req.params.id}`,
                data: result
            })
        }else{
            res.send({ 
                message: 'Not found order with given id'
            })
        }
    }else{
        res.status(500).send({ 
            error: 'Kindly enter a valid id'
        })
    }
}