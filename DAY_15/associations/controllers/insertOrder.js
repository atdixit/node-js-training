import { v4 as uuid } from 'uuid'
import { insertOrder } from '../services/insertOrder.js'

const validate = ( str ) => {
    if(str !== null && str !== ''){
        return true
    }
}

export const handleInsertOrder  = async ( req, res ) => {
    if(req._body && req.body.item_name && req.body.qty && req.body.user_id ){
        const insert_data = {
            order_id: uuid(),
            item_name: req.body.item_name,
            qty: req.body.qty,
            user_id: req.body.user_id
        }
        let flag = true
        for(let i in insert_data){
            if(!validate(i)){
                flag = false
            }
        }
        if(flag){
            const result = await insertOrder( insert_data )
            if(result){
                res.status(200).send({
                    message: `Successfully created a order with order_id: ${result.order_id} of user_id: ${result.user_id}`,
                    data: result
                })
            }else{
                res.status(500).send({
                    error: 'Unable to create the entry, please enter valid info'
                })
            }
        }else{
            res.status(500).send({
                error: 'Unable to create the entry, please enter valid info'
            })            
        }

    }else{
        res.status(500).send({
            error: 'Send a valid body with the request'
        })
    }
}