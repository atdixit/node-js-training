import { getOrdersOfUserId } from "../services/getOrders.js"

export const handleGetOrdersByUserId = async ( req, res) => {
    // console.log('undefinedValue',user_id)
    if(req.params.user_id){
        const result = await getOrdersOfUserId(req.params.user_id)
        if(result && result.length > 0){
            res.send({
                message: "Found entries",
                data: result
            })
        }else{
            res.send({ 
                error: 'Not found orders by given user id'
            })
        }
    }else{
        res.send({
            message: 'Kindly provide a valid user id'
        })
    }
}
