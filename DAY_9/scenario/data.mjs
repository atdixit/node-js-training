const obj = {
    firstName: "Atharva",
    lastName: "Dixit",
    age: 21,
    module: "ES"
};

export {
    obj
};

export default {
    ...obj,default:true
}; // This is the default export of the file.