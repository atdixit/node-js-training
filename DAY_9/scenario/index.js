const obj = require('./data');
const https = require('https');
const axios = require('axios');

console.log("This is an object with usage of CommonJS module default option\n",obj);

// Basic GET request demonstration

axios.get('https://reqres.in/api/products/3')
.then(response => {
    console.log("Via the axios");
    console.log(response.data);
});


https.get('https://reqres.in/api/products/3',res => {
    let data = ""
    res.on('data',d => {
        data += d;
    })

    res.on('end',() => {
        console.log("Via the https\n",data);
    })
})



