import { obj } from './data.mjs';
import objES from './data.mjs';

console.log("This is object with usage of ES module\n",obj);
console.log("This is object with usage of ES default option",objES);