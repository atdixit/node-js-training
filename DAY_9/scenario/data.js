const obj = {
    firstName: "Atharva",
    lastName: "Dixit",
    age: 21,
    module: "CommonJS",
};


module.exports = {...obj,default:true} // This is a default export
exports.a = 9 // This is a simple export but default and simple cannot be accessed at same time.