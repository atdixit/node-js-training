import http from 'http'
import url from 'url'
import { v4 as uuid } from 'uuid'
import { createFile, fileRead, fileWrite } from './fileops.js'

const PORT = 8080

createFile()

const interns = JSON.parse(fileRead())

const sendResponse = ( res, status, data) => {
    res.writeHead(status, { 'Content-Type': 'application/json'})
    res.end(JSON.stringify(data,null,2))
}

const server = http.createServer(( req, res) => {
    // console.log(req)
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE")
    const parsedURL = url.parse(req.url,true)
    // console.log(parsedURL)
    if(parsedURL.pathname === '/interns' && req.method === 'GET'){
        // GET OPERATION ON SERVER
        if(parsedURL.query){
            if(interns.length > 0){
                sendResponse(res, 200, interns.filter(e => e.id === parsedURL.query.id))
            }else{
                sendResponse(res, 400, { message: 'Not Found!'})
            }
        }else{
            sendResponse(res, 200, interns)
        }
    }else if(parsedURL.pathname === '/interns' && req.method === 'POST'){
        // POST OPERATION ON SERVER
        req.on('data', data => {
            const intern = JSON.parse(data)
            if(!intern.name || !intern.age){
                if(!intern.name && intern.name){
                    sendResponse(res,400,{ message: 'No name in request body!'})
                }else if(!intern.age && intern.name){
                    sendResponse(res,400,{ message: 'No age in request body!'})
                }else{
                    sendResponse(res,400,{ message: 'No title and age in request body!'})
                }
            }else{
                interns.push({ id: uuid(), name: intern.name ,age: intern.age })
                fileWrite(res,interns,sendResponse)
            }
        })


    }else if(parsedURL.pathname === '/interns' && req.method === 'PUT'){
        // PUT OPERATION ON SERVER
        req.on('data', data => {
            const id = parsedURL.query.id
            if(parsedURL.query){
                if(id){
                    const newIntern = JSON.parse(data)
                    if(!newIntern.name && newIntern.name){
                        interns.forEach((intern,idx) => {
                            if(intern.id === id){
                                interns[idx].age = newIntern.age
                            }
                        })
                        fileWrite(res,interns,sendResponse)
                    }else if(!newIntern.age && newIntern.name){
                        interns.forEach((intern,idx) => {
                            if(intern.id === id){
                                interns[idx].name = newIntern.name
                            }
                        })
                        fileWrite(res,interns,sendResponse)
                    }else if(newIntern.age && newIntern.name){
                        interns.forEach((intern,idx) => {
                            if(intern.id === id){
                                interns[idx].name = newIntern.name
                                interns[idx].age = newIntern.age
                            }
                        })
                        fileWrite(res,interns,sendResponse)
                    }else{
                        sendResponse(res,400,{ message: 'No name and age in request body!'})
                    }
                }else{
                    sendResponse(res, 400, { message: 'No id param!'})    
                }
            }else{
                sendResponse(res, 400, { message: 'No params!'})
            }
        })

    }else if(parsedURL.pathname === '/interns' && req.method === 'DELETE'){
        // DELETE OPERATION ON SERVER
        if(parsedURL.query.id){
            fileWrite(res,interns.filter(e => e.id != parsedURL.query.id), sendResponse)
        }else{
            sendResponse(res, 400, { message: 'Kindly pass id parameter!'})
        }
    }else{
        sendResponse(res, 404, { message: 'Error: 404 Not Found!'})
    }

    // res.write("Hello from the Server!")
    // res.end()
})

server.listen(PORT, () => {
    console.log(`Server started at port ${PORT}! You can use the URL http://127.0.0.1:8080 to send request.`)
})