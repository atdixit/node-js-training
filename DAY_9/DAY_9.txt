* What is dependencies, npm init and use of package.json file
    dependencies - When we write any JS code or build any project, one might think that do we have to code everything from scratch?
    The answer is no, because JS ecosystem provides various functional code packages ( or simply dependencies ) on which we can rely on.
    For example if we are to develop a React app, we don't need to write React from scratch as it is is already developed by Facebook and we
    just need to install and use it in our project.
    This is what a concept of dependencies means.
    A dependency is any outside or third party code your project uses or depends on. In true sense, a dependency is one on which your project depends,
    such that it cannot function without it is a true dependency.
    For example, if you are making a react app, then without react library you won't be able to function the project.
    Dependency can be said as vast term and making something as a dependency in our project can be done in lot of ways like directly copy-pasting
    a third party code or using it by installing packages.

    A package is generally referred for a code made publicly available and that code can contain single or multiple files.
    Through packages one can add functionality in our project.
    Packages are available on platforms like npm.
    Now npm is a package manager and package repository for JS code.
    package repo is the website where packages are listed and package manager is the CLI which is used to install packages on our local.
    yarn is another package manager which is in some cases better than npm.

    npm stands for Node Package Manager. npm is software registry which contains vast number of packages (of which most are open-source) and it can be used by 
	developers to share software or packages. Also there are many organizations which use npm to manage private development and there are also packages which are 
	privately owned by an organization.
	When talking about npm there are two things which can be referred:
	1. Software registry hosted online
	2. the npm cli which is bundled with npm
	Since npm is a standard package manager for nodejs, it is automatically installed with newer versions of nodejs.

    All your dependencies ( or packages per se) are stored in node_modules and is generally ignored in the project repo ( git repo).
    node_modules folder is not commited in the code as many dependencies which are installed can take up lots of space.
    To avoid node_modules to be carried along with the project, npm creates a package.json file which lists all the details about the project including
    the list of dependencies. This way, if you share your project with another developer, they don't have to download your entire node_modules folder. 
    Instead, once they've copied the project to their own computer, they can run npm install and NPM will reference this file to know which packages (and which versions of those packages) to install.

    Types of dependencies: 
    1. Normal (production) dependencies: These are the ones listed in the "dependencies" key of package.json file.
    They generally contain a name and version key to specify the dependencies but you can also specify:
        1. an approx version: You can use >,<,>=,<=,~ (included all patches),^ (includes all minor changes and patches) for approx versions
        2. a URL: this points to a tarball file so that you can use specific URL
        3. local file: also local file can be used by specifying path after file://
    Normal dependencies will contain anything that your project requires to work in production and that will not be provided by another module.
    So, remember, if it’s absolutely required for your project to work and you can’t rely on another library to include it, then it’s got to go inside your dependencies list.
    These dependencies are downloaded in end browser's cache.
    Examples - react, react-dom, antd etc.
    $ npm install --save <package-name>

    2. Peer dependencies: Whenever we are pulishing or sharing the dependencies, we will need this. Libraries use peer deps to tell other devs which other Libraries with exact version 
    they will need to install in their own project to install your library. So peer dep also express compatibility.
    The peerDependencies ensure that the code is compatible with the version of the package installed. 
    peerDependencies are not automatically installed. 
    You need to manually modify your package.json file in order to add a Peer Dependency.
    Ex - when you are using plugins for bigger libraries.

    3. dev dependencies: These are the dependencies a developer needs when project is in development stage and these are not included in the build or end-browser's 
    cache and when you deploy your project to production, these are of no use. But in dev environment these are very helpful.
    dependencies like eslint,nodemon,husky etc. are dev dependencies
    $ npm install --save-dev <package-name>
    remember that dev dependencies are only used in dev environment so if you manually another type of dependency here, your build won't work.

    4. bundled dependencies: When packages are published, these dependencies are bundled with it. npm packages are available locally or can be downloaded from a single file.
    These are listed as array without versions,  Bundled Dependencies have the same functionality as normal dependencies. When normal dependencies are insufficient, bundled dependencies can be useful. They are rarely used. 
    These are meant for when you’re packaging your project into a single file. This is done through the npm pack command, which turns your folder into a tarball.

    5. optional dependencies: These are just like normal deps and functionality is the same only difference is if npm is unable to install this dep, it will not exit with an error
    unlike normal dep where npm will error out for any reason (network failure, file not found etc.). These are rarely used.
    But when we are using a dep from unknown source and testing it for various purposes we can use this dep.
    $ npm install --save-optional <package-name>
    $ npm install --no-optional <package-name>
    You can manually make a dep optional but remember to handle that part of code as npm will not install in some cases.
    Ex - chalk package

    npm init - 
    The npm init command in the JSON language creates a package.json file for your project’s. 
    A package.json file is a file that contains information about the project’s packages and dependencies. 
    It also contains metadata for the project such as version number, author, and description. 
    It may seem that you can create a package.json in a folder having content just like any other npm package.
    But it is advised and best practice to create it using the npm cli (Command line interface).
    Now, to create a project from scratch make a folder and run the following command:
        $ npm init
        // This command enters an executable environment and ask you for details like name, version description etc. 
        // of the project and you can either provide it or can also skip it but there is a much better way to do this.
        $ npm init -y
        // The -y or --yes flag skips the process of specifying some properties and defaults are set.
        // You can directly edit them in future in package.json file.



    package.json - 
     It is a JSON file which exists in your project's root directory ( or node-project repo as I call it) and is a very important
    file for the NodeJS ecosystem as well as npm or modern JS. 
    For any node-project, package.json is very important and to start development in NodeJS, this is the first step to learn about this file.

    Now, package.json is a JSON file (actual JSON) which stores details (or particularly metadata) about any Node-project or npm package.
    It is often called a manifest file as it contains various sorts of information about a NodeJS-project or npm package in form of project propertieslike
    name, description, version, dependencies, devDependencies, license, scripts etc.

    Because our package.json is only where we record dependencies, and our node_modules/ folder is where the actual code for dependencies is installed, manually updating the dependency field of package.json does not immediately reflect the state of our node_modules/ folder. 
    That's why you want to use npm to help manage dependencies, because it will update both the package.json and node_modules/ folder.

    This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies. 

    You can always edit your package.json manually in your text editor and make changes. That works well for most fields, so long as you're careful not to introduce any JSON formatting errors. We recommend you use the npm CLI commands wherever you can, however.

    The metadata properties can be of the following type :
    1. Identification metadata : These are the properties which are used store Identification details about the project like
    name, version, license, author, description etc. These only provide the basic details of the project and is not related to 
    functioning properties like scripts etc.
    2. Functional metadata : These are the functional properties of the project and contains properties like starting point of the project,
    dependencies, scripts, repo links etc. These are related to project functionality and some are used in direct commands.
    More details are below.

    A default package.json looks like this :
    {
        "name": "assignment-4",
        "version": "1.0.0",
        "description": "",
        "main": "index.js",
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
        },
        "keywords": [],
        "author": "",
        "license": "ISC"
    }

    There can be many more properties added which will be discussed later.

    Basic properties included in the package.json :
    name : sets the application/package or project name. It should be less than 214 characters, no spaces, lowercase letters only,
    hyphen and underscores are allowed. Other specials chars are not allowed. By this name it is published through this name.

    "name": "test-project"

    description : It basically specifies what the project is about and what it does generally a brief what-about of your project.
        "description": "A test project for learning"

    version : It indicates the version of the project.
    This uses the semantic version which means the version is always like : x.x.x
    It takes the form of major.minor.patch where major, minor, and patch are integers which increase after each new release.
    Ex :
    "version": "16.13.1"

    license : It indicates the license of the package or project. Default license is ISC.
    If you don't wish to provide a license, or explicitly do not want to grant use of a private or unpublished package, you can put "UNLICENSED" as the license.

    author : It specifies the author name like this :
    {
        "author": "Atharva <atdixit@gammastack.com> (https://sample.com)"
    }
    or 
    {
        "author": {
            "name": "Atharva",
            "email": "atdixit@gammastack.com",
            "url": "https://sample.com"
        }
    }


    keywords : It is an array of strings which are words through which it can be searched on the NPM registry.
    This allows people who find your project understand what it is in just a few words.

    "keywords": [
        "demo",
        "demo test"
    ]

    homepage : The url to the project homepage.
        "homepage": "https://github.com/owner/project#readme"

    repository: Specifies where your project remote repo is located. This is helpful for people who want to contribute. 
    If the git repo is on GitHub, then the npm docs command will be able to find you.
    {
    "repository": {
        "type": "git",
        "url": "https://gitlab.com/atdixit"
        }
    }

    main : The main field is a functional property of your package.json. 
    This defines the entry point into your project, and commonly the file used to start the project.
    This is commonly an index.js file in the root of your project, but it can be any file you choose to use as the main entry-point to your package.
        "main": "index.js"

    engines : Sets which versions of Node.js and other commands this package/app work on.
        "engines": {
            "node": ">= 6.0.0",
            "npm": ">= 3.0.0",
            "yarn": "^0.13.0"
        }



* Module in nodeJS i.e use of require
    In NodeJS, modules are codes which communicates with an external application on the basis of
    related functionality. It can be a single file or a collection of files and folders.
    You can also consider modules just like JS libraries, a set of functions you want to use in your code.
    More formally, In Node.js, a module is a collection of JavaScript functions and objects that can be used by external applications. 
    Describing a piece of code as a module refers less to what the code is and more to what it does—any Node.js file or collection of files can be considered a module if its functions and data are made usable to external programs.
    These are very useful because of various reasons like :
    1. These are tested codes and can be re-used in our code.
    2. Breaking down a complex code into simpler pieces.
    3. Various functionality you don't need to implement from scratch as there are already modules for it.

    There are 3 types of modules in NodeJS :
    1. Core Modules - These are built-in modules of NodeJS which are part of the NodeJS platform. They provide various basic
    functionality in NodeJS. Some of them are as follows :
        1. http - Helps to create an HTTP server in NodeJS.
        2. fs - used to handle the file system.
        3. path - includes methods to deal with the file paths.
        4. process - provides info and control of current NodeJS process.
        5. url - helps in URL resolution and parsing.
    And there are many more Core modules in NodeJS.
    2. Local Modules - These are user created modules in NodeJS. Functions, variables, objects, classes etc. can be exported
    from a file to make available for the whole project. export keyword / modules.exports is used for the same purpose.
    Since this file provides attributes to the project via exports, another file can use its exported functionality using the require() function.
    It can also be done using the module API of NodeJS.
    3. Third-party modules - These are external modules which are available online which are either created by developer/s or tech-companies
    which are hosted on various sites like npm registry or yarn. These can be installed in the project folder or globally.
    Some popular third-party modules are :
        1. express
        2. angular
        3. react
    These are very useful as we know that. These can be install using the npm install command inside an npm repo or globally.

    Also every file in NodeJS is considered a module.

    Now that we saw what modules are and what are its types, now we will see what are module systems.
    There are various types of Module systems in NodeJS :
    1. CommonJS module system : By default NodeJS treats JS code as CommonJS modules and hence CommonJS is default module system 
    of NodeJS. In NodeJS module system, each file is treated as a seperate module.
    CommonJS modules are characterized by the require() statement for module imports and module.exports for module exports.
    Since they are default in NodeJS, we can simply use them in .js files.
    But they only work synchronously and hence are blocking.
    Ex -
        // export 
        module.exports.add = function(a, b) {
                return a + b;
        } 

        module.exports.subtract = function(a, b) {
                return a - b;
        } 

        // import 
        const {add, subtract} = require('./util')

        console.log(add(5, 5)) // 10
        console.log(subtract(10, 5)) // 5


    2. ECMAScript or ES module system : They are the official standard format to package JavaScript code for reuse. Modules are defined using a variety of import and export statements.
    ES modules uses static import and export statement for this.
    To use ES module you have to change the file extension to .mjs or you can specify type property as module in package.json
    so that you can use them in your code.
    Some benefits of ES modules are Tree Shaking (removal of dead code), encapsulation, works on server and client-side.
    Example implemented in index.mjs/data.mjs file.
    3. AMD module system : It stands for Asynchronous module definition and it was releases by developers of RequireJS.
    The AMD API is designed for the browser, it is non-blocking thus, it works asynchronously.
    AMD is good to use, works in browser and work asynchronously but we need a module loader like RequireJS to use it.
    Also, it uses a wrapper function for encapsulation. 
    The AMD specification defines a single function called define with the following function signature:
    define(id?, dependencies?, factory);
        id is an optional argument that specifies the module being defined.
        Dependencies refer to an array of the module ids of the dependencies being required.
        Factory refers to the callback function that should be called to instantiate the module. The returned value of this function is the exported value of the module.
    Ex - 
        define(['dep1', 'dep2'], function (dep1, dep2) {
            //methods
            function foo (){}; // public because it is returned
            function bar (){}; // public because it is returned
            function jar (){}; // private becasue it is not returned

            return {
            foo: foo,
            bar: bar
        }
        });



    Use of require in detail :
    Node.js follows the CommonJS module system, and the builtin require function is the easiest way to include modules that exist in separate files. 
    The basic functionality of require is that it reads a JavaScript file, executes the file, and then proceeds to return the exports object. 
    The rules of where require finds the files can be a little complex, but a simple rule of thumb is that if the file doesn't start with "./" or "/", then it is either considered a core module (and the local Node.js path is checked), or a dependency in the local node_modules folder. 
    If the file starts with "./" it is considered a relative file to the file that called require. If the file starts with "/", it is considered an absolute path. NOTE: you can omit ".js" and require will automatically append it if needed.
    If the filename passed to require is actually a directory, it will first look for package.json in the directory and load the file referenced in the main property. 
    Otherwise, it will look for an index.js.
    The require function will look for files in the following order
        1. Built-in core Node.js modules (like fs, path)
        2. NPM Modules. It will look in the node_modules folder.
        3. Local Modules. If the module name has a ./, / or ../, it will look for the directory/file in the given path. It matches the file extensions: *.js, *.json, *.mjs, *.cjs, *.wasm and *.node.

    The main object exported by require() module is a function.  When Node invokes that require() function with a file path as the function’s only argument, Node goes through the following sequence of steps:
        Resolving and Loading
        Wrapping
        Execution
        Returning Exports
        Caching


* Implement following scenario:
    - Create 2 files data.js, index.js
    - Define an object in data.js and use export or module.export to export the object.
    - Write logic to print the object data in index.js

    Implemented in the scenario folder.
    
* Create server and basic get, put, post and delete APIs.  [without using express framework]

