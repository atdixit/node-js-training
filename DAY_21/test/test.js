import chai, { assert } from 'chai'
import { addition, subtraction, multiplication, division } from './index.js'

const expect = chai.expect
const asset = chai.assert

describe('ARITHMETIC',() => {
    describe('ADD', () => {
        it('should add two nums', () => {
            expect(addition(1,1)).to.equal(2)
            expect(addition(10,14)).to.equal(24)
            
        })
    })

    // This test case will fail, this is for failure cases
    // describe('ASSERT_ADD', () => {
    //     let a = addition(10,14)
    //     assert.equal(a,23, 'checking add with assert')
    // })

    describe('SUBTRACT', () => {
        it('should subtract two nums', () => {
            expect(subtraction(1,1)).to.equal(0)
            expect(subtraction(10,14)).to.equal(-4)
        })
    })

    describe('MULTIPLY', () => {
        it('should multiply two nums', () => {
            expect(multiplication(1,5)).to.equal(5)
            expect(multiplication(10,14)).to.equal(140)
        })
    })

    describe('DIVIDE', () => {
        it('should divide two nums', () => {
            expect(division(25,5)).to.equal(5)
            expect(division(10,0)).to.equal(undefined)
        })
    })
})