//Examples of let and const scope rules :
let x = 1
if(true){
    let x = 2
    console.log(x) //OP is 2
}

console.log(x) // OP is 1

function varTest() {
    var x = 1;
    {
        var x = 2;  // same variable!
        console.log(x);  // 2
    }
    console.log(x);  // 2
}

function letTest() {
    let x = 1;
    {
        let x = 2;  // different variable!
        console.log(x);  // 2
    }
    console.log(x);  // 1
}
// let does not create a global property while var does.
x = 1
var y = 2
console.log(this.x)
console.log(this.y)

const a = 1
//a = 2 //Throws an error of invalid Assignment to Constant

/* OP of the file.
2
1
undefined
undefined
*/
