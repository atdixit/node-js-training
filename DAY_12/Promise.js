let myPromise = new Promise((resolve,reject) => {
    setTimeout(() => resolve('Promise fulfilled.'),300);
    setTimeout(() => reject(new Error('Rejected!')),400);
})

myPromise.then((res) => {
    console.log(res);
}).catch(err => {
    console.log(err);
}).finally(() => {
    console.log("Finally Executed!");
})

Promise.reject(new Error("Rejected Second Promise!")).then((res)=>{
    console.log("Fullfilled!");
}).catch((err)=>console.error(err));

console.log("This is consoled after promise but it is printed first!");

/*
This is consoled after promise but it is printed first!
Error: Rejected Second Promise!
    at Object.<anonymous> (/home/gammastack/Desktop/atharva-dixit/Assignment-2-2-BasicJS/Promise.js:14:16)
    at Module._compile (node:internal/modules/cjs/loader:1101:14)
    at Object.Module._extensions..js (node:internal/modules/cjs/loader:1153:10)
    at Module.load (node:internal/modules/cjs/loader:981:32)
    at Function.Module._load (node:internal/modules/cjs/loader:822:12)
    at Function.executeUserEntryPoint [as runMain] (node:internal/modules/run_main:81:12)
    at node:internal/main/run_main_module:17:47
Promise fulfilled.
Finally Executed!
*/

/* When we are using asynchornous JS then outputs are not necessarily sequential.
*/

// New additions for fetch API calls 

const btn = document.getElementById("click-to-fetch");
const p = document.getElementById("response-data");

function fetchAPI(){
    fetch('https://reqres.in/api/products/3')
    .then(response => {
        return response.text();
    })
    .then(html => {
        p.innerHTML = html;
        console.log(html);
    })
    .catch(err => console.error(err))
    .finally(()=>{
        console.log("FINALLY EXECUTED!");
    });
}

btn.addEventListener('click',() => fetchAPI());