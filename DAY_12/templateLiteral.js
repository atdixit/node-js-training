
const firstName = "Atharva";


/*
We can embed a variable or any expression in the below substitution so that a value can be replaced there.

*/
console.log(`Hello ${firstName}`); //Hello Atharva

console.log('`' == `\``); // true because untagged template literals returns strings.

// Multi-Line strings in ES5

/*
In ES5 we have to use the escape sequence \n to create multi-line string or \ should be put if we are 
line breaking a string.
*/
console.log("Multi line \n"+"string in \
        ES5");

// Template literal form of multi-line strings

console.log(`Multi line
string
in 
template
literals

`);

// Tagged template literals in ES6

function tagFunc(arrOfString, firstName, age){
    return `${arrOfString[0]}${firstName}${arrOfString[1]}${age}${arrOfString[2]}`;
}

let age = 21;

let output = tagFunc`Hello ${firstName}! Your age is ${age}.`;

console.log(output); //Hello Atharva! Your age is 21.


// Raw strings

// This is a simple template string
console.log(`This is a \n multi line string`);
/*
This is a 
 multi line string
*/

console.log(String.raw`This is a \n multi line string`); //This is a \n multi line string


/* Collective output of the file
Hello Atharva
true
Multi line 
string in         ES5
Multi line
string
in 
template
literals


Hello Atharva! Your age is 21.
This is a 
 multi line string
This is a \n multi line string

*/
