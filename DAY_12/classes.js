// Class declaration

class A{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }
}

// Hoisting is not allowed in classes

// Class expressions

let B = class C{

    constructor(x,y){
        this.x = x;
        this.y = y;
    }

    get getX(){ // Prototype method
        return this.x;
    }

    static classvariable = 12;
}

console.log(B.name);

console.log(B.classvariable);


class D extends A {
    constructor(x,y){
        super(x,y);
    }
}

let obj = new D();

console.log(obj.x,obj.y);



/*
C
12
undefined undefined
*/