

const arr = [1,2,3,4,5];

const str = "gammastack";

const st = new Set([1,1,2,2,3,3,4,4]);


for(const elem of arr){
    console.log(elem);
}

/*
1
2
3
4
5
*/

for(const elem of str){
    console.log(elem);
}

/*
g
a
m
m
a
s
t
a
c
k
*/

for(const elem of st){
    console.log(elem);
}

/*
1
2
3
4
*/
// Function arguments.
(function() {
    for (const argument of arguments) {
        console.log(argument);
    }
})(1, 2, 3);

// 1
// 2
// 3

// Generator functions
function* foo(){
    yield 1;
    yield 2;
    yield 3;
};
  
for (const o of foo()) {
console.log(o);
break;
}
console.log('done'); // done