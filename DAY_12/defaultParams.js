
//  A simple add function with default argument
const add = (a,b = 1) => {
    return a+b;
}

console.log(add(1,2)); // 3

console.log(add(1)); // 2

console.log(add(1,undefined)); //2

console.log(add(undefined)); // NaN


// If the same function was to be written in ES5 it would be like this
function addES5(a,b){
    if( b == undefined ){
        b = 1;
    }
    return a+b;
}

console.log(addES5(1,2)); // 3

console.log(addES5(1)); // 2

console.log(addES5(1,undefined)); //2

console.log(addES5(undefined)); // NaN

// When tested with falsy values
function test( num = 10){
    console.log(typeof num);
}

test(); // number
test(undefined); // number
test(''); // string
test(null); // object
test(NaN); // number

// The evaluation is done at the call time of the function

const addToArray = (value, array = []) => {
    array.push(value)
    console.log(array);
    return array;
}

addToArray(1); // [1]
addToArray(5); // [5] and not [1,5]

// Earlier parameters available to default parameters
function sayHello(name, id = `${name}01`){
    console.log(`Hi ${name}, your id is ${id}`);
}

sayHello("Atharva"); // Hi Atharva, your id is Atharva01
sayHello("Atharva","02363"); // Hi Atharva, your id is 02363


// You cannot use a function with default param as it is not accessible in function body

function HELLO(name,hello = () => {console.log(`Hello ${name}`);}){
    var name = "REACTJS";
    hello(); // name is not altered to REACTJS as they exist in their own scope
}

HELLO("Atharva"); // Hello Atharva

// Always use the default params at right to avoid over-writing of the normal parameters


/* Collective output of the file
3
2
2
NaN
3
2
2
NaN
number
number
string
object
number
[ 1 ]
[ 5 ]
Hi Atharva, your id is Atharva01
Hi Atharva, your id is 02363
Hello Atharva

*/
