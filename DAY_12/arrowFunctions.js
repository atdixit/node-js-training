// We are going to use IIFE (Immediately Invoked Function Expressions) here for faster execution

console.log((function(a){
    return a*5;
})(200));
// 1000

// Break down of the arrow function

//Remove the function keyword and place an arrow between ) and { 
console.log(((a) => {
    return a*5;
})(200));
// 1000

// Remove the body braces as they are not needed for single statement and remove the return word as return is implied.

console.log(((a) => a+100)(200)); // 1000

// With Arguments

console.log(((a,b) => a*b)(15,7)); //105

// Named functions

const add = (a,b) => a+b;
console.log(add(5,7)); //12

// Rest and default parameters are supported

const another = (a = 1, ...params) => {
    console.log(a, params);
}
another(undefined,1,2,3); //1 [ 1, 2, 3 ]


const obj = {
    age: 21,
    func1: function(){
        console.log(this.age);
    },

    func2: () => {
        console.log(this.age);
    }
}

obj.func1(); // 21

obj.func2(); // undefined

let foo = () => "foo";
let foo2 = function(){
    return "foo2";
}

// let bar = new foo(); // TypeError 
// new keyword cannot be used with arrow functions

let bar2 = new foo2();

console.log(bar2); // foo2 {}

/* Collective output of the file
1000
1000
300
105
12
1 [ 1, 2, 3 ]
21
undefined
foo2 {}

*/