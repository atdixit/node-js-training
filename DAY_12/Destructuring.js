
const arr = [1,2,3,4,5];

let [x,y,...rest] = arr;  //Destructuring assignment

console.log(x,y,rest) // 1 2 [3 4 5]

let a = 2,c = 3;
[a,c]=[c,a]; // Swapping variables
console.log(a,c);

console.log(x,y);

const obj = {
    a:1,
    b:undefined,
    c:3
}

const {a:aa,b = 12} = obj; // We can also name them and assign them default values

console.log(aa,b);


function f({a,b = 13}){
    return [a,b];
}

const [m] = f(obj);
console.log(m);

/* OP of the file
1 2 [ 3, 4, 5 ]
3 2
1 2
1 12
1
*/