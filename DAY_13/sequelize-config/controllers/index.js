import { handleInsertUser } from "./insertUser.js"
import { handleGetUser, handleGetAllUsers } from "./getUser.js"
import { handleDeleteUser, handleDeleteAllUser } from "./deleteUser.js"

export {
    handleInsertUser,
    handleGetUser,
    handleGetAllUsers,
    handleDeleteAllUser,
    handleDeleteUser
}