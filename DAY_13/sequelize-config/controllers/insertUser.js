import { insertUser } from "../services/insertUser.js"

const validate = ( str ) => {
    if(str !== null && str !== ''){
        return true
    }
}

export const handleInsertUser = async ( req, res ) => {
    if(req._body && req.body.name && req.body.city && req.body.education ){
        const insert_data = {
            name: req.body.name,
            city: req.body.city,
            education: req.body.education
        }
        let flag = true
        for(let i in insert_data){
            if(!validate(i)){
                flag = false
            }
        }
        if(flag){
            const result = await insertUser( insert_data )
            if(result){
                res.status(200).send({
                    message: `Successfully created a user with id: ${result.id}`,
                    data: result
                })
            }else{
                res.status(500).send({
                    error: 'Unable to create the entry, please enter valid info'
                })
            }
        }else{
            res.status(500).send({
                error: 'Unable to create the entry, please enter valid info'
            })            
        }

    }else{
        res.status(500).send({
            error: 'Send a body with the request'
        })
    }
}