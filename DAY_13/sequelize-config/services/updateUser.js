import { User } from "../models/User.js"

export const updateUserById = async ( id, updated_data ) => {
    const result = await User.update(updated_data, {
        where: {
            id
        },
        returning: true
    })
    return result
}