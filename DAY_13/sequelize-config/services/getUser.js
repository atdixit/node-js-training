import { User } from "../models/User.js"

export const getUserById = async (id) => {
    const user = await User.findOne({ where: { id }})
    return user
}

export const getUsers = async () => {
    const users = await User.findAll()
    return users
}