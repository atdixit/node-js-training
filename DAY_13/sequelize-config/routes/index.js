import express from 'express'
import { basicAuth } from '../auth/index.js'
import { handleDeleteAllUser, handleDeleteUser } from '../controllers/deleteUser.js'
import { handleInsertUser, handleGetUser, handleGetAllUsers } from '../controllers/index.js'
import { handleUpdateUserById } from '../controllers/updateUser.js'
import { syncOnly } from '../sequelize/index.js'

const router = express.Router()

router.get('/get-user/:id',handleGetUser)
router.get('/get-all-users', basicAuth , handleGetAllUsers)
router.post('/insert-user', basicAuth ,handleInsertUser)
router.put('/update-user/:id', basicAuth ,handleUpdateUserById)
router.delete('/delete-user/:id', basicAuth, handleDeleteUser,syncOnly)
router.delete('/delete-all-users',basicAuth, handleDeleteAllUser,syncOnly)

export default router