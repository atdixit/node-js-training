import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors'
import 'dotenv/config'

import { connect } from './sequelize/index.js'
import router from './routes/index.js'

const PORT = process.env.PORT || 8081
const corsOptions = {
    origin: `http://localhost:${PORT}`
}

const app = express()
connect()

app.use(bodyParser.json())
app.use(cors(corsOptions))

const setJSON = (req,res,next) => {
    res.setHeader('Content-Type','application/json')
    next()
}

app.use('/',setJSON,router)

app.get('/getusers/:id', ( req, res) => {
    console.log('Iddddddd:',req.params.id)
    res.send({
        message: "Request received!"
    })
})
let a = new Buffer.from('root:enigma@123').toString('base64')
console.log(`Console Basic ${a}`)

app.listen(PORT, () => {
    console.log(`Listening at PORT ${PORT}, kindly use the link http://localhost:${PORT}/`)
})