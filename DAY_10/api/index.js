import express from 'express'
import bodyParser from 'body-parser'
import { createFile, fileRead, fileWrite } from './fileops.js'
import { v4 as uuid } from 'uuid' 

const PORT = 8080
createFile()

const app = express()
let users = JSON.parse(fileRead())

const sendResponse = ( res, status, data) => {
    res.writeHead(status, { 'Content-Type': 'application/json'})
    res.end(JSON.stringify(data,null,2))
}

const logger = (req, res, next) => {
    console.log("Request Received!")
    console.log(req)
    next()
}

app.use(bodyParser.json())
app.use(logger)

app.get('/api/users',( req, res ) => {
    users = JSON.parse(fileRead())
    sendResponse(res, 200, users) 
})

app.post('/api/users',( req, res ) => {
    const body = req.body
    users = JSON.parse(fileRead())
    if( !body.name || !body.city || !body.education){
        sendResponse(res, 400, { message: 'Kindly pass all the parameters i.e. name, city and education'})
    }else{
        users.push({ id: uuid() , ...body })
        fileWrite(res, users, sendResponse)
    }
})

app.put('/api/users', ( req, res ) => {
    users = JSON.parse(fileRead())
    users.forEach((elem,idx) => {
        if(elem.id === req.query.id){
            if(req.body.name){
                elem.name = req.body.name
            }
            if(req.body.city){
                elem.city = req.body.city
            }
            if(req.body.education){
                elem.education = req.body.education
            }
            if(!req.body){
                sendResponse(res, 400, { message: 'Empty body'})
            }
        }
    })
    fileWrite(res, users, sendResponse)
})

app.delete('/api/users', ( req, res ) => {
    users = JSON.parse(fileRead())
    console.log(req.query.id)
    users.filter(elem => elem.id !== req.query.id)
    fileWrite(res, users, sendResponse)
})




app.listen(PORT, () => {
    console.log(`Listening at PORT ${PORT}`)
})