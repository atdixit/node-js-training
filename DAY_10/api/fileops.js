import fs from 'fs'

const path = './users.json';

const fileExists = () => {
    try {
        if (fs.existsSync(path)) {
            return true;
        }
        return false;
    } catch (err) {
        console.error(err);
        return false;
    }
};

const createFile = () => {
    if (!fileExists()) {
        fs.writeFileSync(path, JSON.stringify([]))
    } else {
        console.log('File already exists...')
    }
}

const fileRead = () => {
    return fs.readFileSync(path)
}

const fileWrite = (res, data, sender) => {
    const jsondata = JSON.stringify(data, null, 2)

    fs.writeFile(path, jsondata, (err) => {
        if (err) {
            sender(res, 500, { message: 'Error in writing data ...' })
        } else {
            sender(res, 200, data)
        }
    })
}

export {
    createFile,
    fileWrite,
    fileRead
}