import Client from "../models/Clients.js"

export const createClient = async ( insert_data ) => {
    const result = await Client.create({
        ...insert_data,
    })
    return result
}