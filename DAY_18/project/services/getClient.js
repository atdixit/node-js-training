import Client from '../models/Clients.js'

export const getClientIdPassword = async ( username ) => {
    const result = await Client.findOne({
        where: {
            username
        }
    })
    return result
}