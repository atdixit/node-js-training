import express from 'express'

import { loginController } from '../controllers/login.js'
import { signUpController } from '../controllers/signup.js'
import { authenticate } from '../middleware/authentication.js'

const router = express.Router()

const validation = ( req, res ) => {
    res.send({
        message: 'Validation done!'
    })
}

router.get('/validate',authenticate, validation)
router.get('/login',loginController)
router.post('/signup',signUpController)

export default router