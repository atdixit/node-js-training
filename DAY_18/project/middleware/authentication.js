import jwt from 'jsonwebtoken'

export const authenticate = ( req, res, next ) => {
    let jwtSecretKey = process.env.JWT_SECRET_KEY
  
    try {
        const token = req.headers.authorization.split(' ')[1];
        const verified = jwt.verify(token, jwtSecretKey);
        if(verified){
            next()
        }else{
            return res.status(401).send({
                error: "Enter valid token"
            });
        }
    } catch (error) {
        return res.status(401).send({
            error: `${error}`
        });
    }
}