import { DataTypes } from "sequelize"
import { sequelize } from "../sequelize/index.js"

const Client = sequelize.define('Client',{
    username: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING
    },
    mobileno: {
        type: DataTypes.STRING
    }
},{
    tableName: "clients",
    timestamps: false
})

export default Client