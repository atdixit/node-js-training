import express from 'express'

import 'dotenv/config'

import routes from './routes/index.js'
import { connect } from './sequelize/index.js'
import { logger } from './middleware/logger.js'

const PORT = process.env.PORT || 8080

const app = express()
connect()

app.use(express.json())
app.use(logger)
app.use('/',routes)

app.listen(PORT, () => {
    console.log(`Server listening at PORT ${PORT}`)
})