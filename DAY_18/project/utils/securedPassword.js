import crypto from 'crypto'

export const generateSecurePassword = (password) => {
    return crypto.createHash('sha256').update(password).digest('hex')
}
