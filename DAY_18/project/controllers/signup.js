import { createClient } from '../services/createClient.js'
import { generateSecurePassword } from "../utils/securedPassword.js"

const validate = ( str ) => {
    if(str !== null && str !== ''){
        return true
    }
}

export const signUpController = async ( req, res ) => {
    if(req._body && req.body.username && req.body.password && req.body.address && req.body.mobileno ){
        const insert_data = {
            username: req.body.username,
            password: generateSecurePassword(req.body.password),
            address: req.body.address,
            mobileno: req.body.mobileno,
        }
        let flag = true
        for(let i in insert_data){
            if(!validate(i)){
                flag = false
            }
        }
        if(flag){
            const result = await createClient( insert_data )
            if(result){
                res.status(200).send({
                    message: `Successfully created a user with username: ${result.username}`,
                    data: result
                })
            }else{
                res.status(500).send({
                    error: 'Unable to create the entry, please enter valid info'
                })
            }
        }else{
            res.status(500).send({
                error: 'Unable to create the entry, please enter valid info'
            })            
        }

    }else{
        res.status(500).send({
            error: 'Send a valid body with the request'
        })
    }
}