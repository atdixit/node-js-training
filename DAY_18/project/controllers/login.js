import jwt from 'jsonwebtoken'
import { getClientIdPassword } from '../services/getClient.js'
import { generateSecurePassword } from '../utils/securedPassword.js'

export const loginController = async ( req, res ) => {
    if(req._body){
        if(req.body.username === process.env.ADMIN_USERNAME && req.body.password === process.env.ADMIN_PASS){
            const token = jwt.sign({
                id: 'admin',
                name: 'admin'
            }, process.env.JWT_SECRET_KEY,{
                expiresIn: '1d'
            })
            res.status(201).send({
                message: 'Admin token created!',
                token
            })
            return
        }
        if(req.body.username && req.body.password){
            const result = await getClientIdPassword(req.body.username)
            if(result && result.password === generateSecurePassword(req.body.password)){
                const jwt_payload = {
                    username: result.username,
                    password: result.password
                }
                const token = jwt.sign(jwt_payload, process.env.JWT_SECRET_KEY,{
                    expiresIn: '1d'
                })
                // console.log('Token',token)
                res.status(201).send({
                    message: 'Token successfully created',
                    token
                })
            }
            else{
                res.status(403).send({
                    error:'Enter valid id and password'
                })
            }
        }else{
            res.status(403).send({
                error: 'Enter valid id and password'
            })
        }
    }else{
        res.status(403).send({
            error: 'Enter a valid body'
        })
    }    
}