* axios
    The original documentation of axios defines itself as "Axios is a promise-based HTTP Client for node.js and the browser. It is isomorphic (= it can run in the browser and nodejs with the same codebase). On the server-side it uses the native node.js http module, while on the client (browser) it uses XMLHttpRequests."
    Various features of axios are :
    1. Make XHR requests from browser
    2. Make http requests from node.js
    3. Supports the Promise API
    4. Intercept request and response
    5. Transform request and response data
    6. Cancel requests
    7. Automatic transforms for JSON data
    
    Now, in technical terms it is a dependency and can be installed in the following way:
    $ npm install axios
    or you can directly add it to the script
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    // POST request
    axios({
        method: 'post',
        url: '/user/12345',
        data: {
            firstName: 'Fred',
            lastName: 'Flintstone'
        }
    });
    also there are particular request methods:
        axios.request(config)
        axios.get(url[, config])
        axios.delete(url[, config])
        axios.head(url[, config])
        axios.options(url[, config])
        axios.post(url[, data[, config]])
        axios.put(url[, data[, config]])
        axios.patch(url[, data[, config]])

    Also we can create an axios instance and use it for various purposes :
    const instance = axios.create({
        baseURL: 'https://some-domain.com/api/',
        timeout: 1000,
        headers: {'X-Custom-Header': 'foobar'}
    });
    Instance methods :
        axios#request(config)
        axios#get(url[, config])
        axios#delete(url[, config])
        axios#head(url[, config])
        axios#options(url[, config])
        axios#post(url[, data[, config]])
        axios#put(url[, data[, config]])
        axios#patch(url[, data[, config]])
        axios#getUri([config])


    Differences between axios and fetch :
    There are various differences in axios and fetch and can be categorised as follows :
    1. Syntax differences:
        1. To send data, fetch() uses the body property for a post request to send data to the endpoint, while Axios uses the data property
        2. The data in fetch() is transformed to a string using the JSON.stringify method
        3. Axios automatically transforms the data returned from the server, but with fetch() you have to call the response.json method to parse the data to a JavaScript object.
        4. With Axios, the data response provided by the server can be accessed with in the data object, while for the fetch() method, the final data can be named any variable
    2. One of the most important point in connection with axios is that it is having a wide browser support. Even old browsers like IE11 can run Axios without any issue. This is because it uses XMLHttpRequest under the hood.
    Fetch(), on the other hand, only supports Chrome 42+, Firefox 39+, Edge 14+, and Safari 10.3+. If your only reason for using Axios is backward compatibility, you don’t really need an HTTP library.
    3. The simplicity of setting a timeout in Axios is one of the reasons some developers prefer it to fetch(). In Axios, you can use the optional timeout property in the config object to set the number of milliseconds before the request is aborted.
    4. One of the key features of Axios is its ability to intercept HTTP requests. HTTP interceptors come in handy when you need to examine or change HTTP requests from your application to the server or vice versa (e.g., logging, authentication, or retrying a failed HTTP request). With interceptors, you won’t have to write separate code for each HTTP request. HTTP interceptors are helpful when you want to set a global strategy for how you handle request and response.
    5. Progress indicators are very useful when loading large assets, especially for users with slow internet speed. Previously, JavaScript programmers used the XMLHttpRequest.onprogress callback handler to implement progress indicators. The Fetch API doesn’t have an onprogress handler. Instead, it provides an instance of ReadableStream via the body property of the response object.
    6. To make multiple simultaneous requests, Axios provides the axios.all() method. Simply pass an array of requests to this method, then use axios.spread() to assign the properties of the response array to separate variables. You can achieve the same result by using the built-in Promise.all() method. Pass all fetch requests as an array to Promise.all(). Next, handle the response by using an async function

* issue in GitLab
    Issues in gitlab are basically anything or any discussion related to your project. We can use issues to collaborate on ideas, solve problems, and plan work. Share and discuss proposals with your team and with outside collaborators. You can use issues for many purposes, customized to your needs and workflow.
        1. Discuss the implementation of an idea.
        2. Track tasks and work status.
        3. Accept feature proposals, questions, support requests, or bug reports.
        4. Elaborate on code implementations.
    The benefits of collaboration start at the point of making an issue. By making an issue, you get your ideas out there, and this allows your collaborators to have their say early in the process. Non-programmers think that programmers are constantly tapping away at their keyboards on code. In fact software development is more like a discussion, and it’s a more collaborative experience than it is a solitary one. The tools GitLab provides make it easier to manage that discussion, and keep the flow of conversation moving.

    By starting with an issue you also avert a number of risks which can introduce problems later on.
        1. You may not know everything there is to consider.
        2. There may be parts of the system you aren’t familiar with.
        3. There may be limitations or possibilities you’re not aware of.
        4. There may be factors for users you may not know.
        5. There may be work going on in a parallel effort which relates your idea.
    Also, for creating a gitlab issue, you must be atleast a Guest to the repo.
* Read about api provided by GitLab
    Gitlab provides an api which allows you to perform many of the actions you typically do when using the user interface. Using the API allows us to automate the process and eliminate any user interaction.

* Implement api to create issue in gitlab
    done in project.
