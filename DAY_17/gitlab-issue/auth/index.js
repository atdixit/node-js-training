export const auth = ( req, res, next ) => {
    // console.log('Authorization',req.headers.authorization)
    if(req.headers.authorization){
        const a = `Bearer ${process.env.ACCESS_TOKEN}`
        if(a === req.headers.authorization){
            // console.log('Auth Successful!')
            next()
        }else{
            res.send({ 
                message: 'Auth Failed! Kindly check the valid access token.'
            })
        }
    }else{
        res.send({ 
            error: 'You will need a access token to proceed!'
        })
    }
}