import instance from "../config/axios.js"

export const createIssue = async (req, res) => {
    if (req.params.id) {
        const { data } = await instance.post(`/projects/${req.params.id}/issues`)
        res.send(data)
    } else {
        res.send({
            error: 'Kindly use a valid project id'
        })
    }
}