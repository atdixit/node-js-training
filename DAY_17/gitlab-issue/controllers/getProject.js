import instance from "../config/axios.js"

export const getProject = async ( req, res) => {
    if(req.params.id){
        const { data } = await instance.get(`/projects/${req.params.id}`)
        res.send(data)
    }else{
        res.send({
            error: 'Kindly use a valid project id'
        })
    }
}