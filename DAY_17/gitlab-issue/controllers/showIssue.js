import instance from "../config/axios.js"

export const showIssue = async (req, res) => {
    // console.log('Hello')
    if (req.params.project_id) {
        const { data } = await instance.get(`/projects/${req.params.project_id}/issues`)
        console.log('DATA',data)
        res.send(data)
    } else {
        res.send({
            error: 'Kindly use a valid project id'
        })
    }
}