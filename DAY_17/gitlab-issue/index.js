import express from 'express'

import 'dotenv/config'

import routes from './routes.js'
import { setJSON } from './middleware/setJson.js'
import { genericRequestLogger } from './middleware/genericRequestLogger.js'
import { auth } from './auth/index.js'

const PORT = process.env.PORT || 8080

const app = express()

app.use(auth)
app.use(setJSON)
// app.use(genericRequestLogger)
app.use('/',routes)

app.listen(PORT,() => {
    console.log(`Server listening on PORT ${PORT}`)    
})