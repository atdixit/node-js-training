import express from 'express'
import { createIssue } from './controllers/createIssue.js'
import { getProject } from './controllers/getProject.js'
import { showIssue } from './controllers/showIssue.js'

const router = express.Router()

router.get('/get-project/:id',getProject)
router.get('/show-issues/:project_id',showIssue)
router.post('/create-issue',createIssue)


export default router