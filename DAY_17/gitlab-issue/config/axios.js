import axios from "axios"

const instance = axios.create({
    baseURL: process.env.BASE_URL,
    timeout: 4000,
    headers: {
        'Authorization': `Bearer ${process.env.ACCESS_TOKEN}`
    }
})

export default instance