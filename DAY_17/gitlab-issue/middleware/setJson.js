export const setJSON = ( _, res, next) => {
    res.setHeader('Content-Type','application/json')
    next()
}