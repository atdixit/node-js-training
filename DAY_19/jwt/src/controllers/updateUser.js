import { updateUserById } from "../services/updateUser.js"
import { generateSecurePassword } from "../utils/securedPassword.js"

export const handleUpdateUserById = async ( req, res ) => {
    if(req.params.id && req._body){
        if(req.body.password){
            req.body.password = generateSecurePassword(req.body.password)
        }
        const result = await updateUserById(req.params.id,req.body)
        if(result){
            res.send({
                message: `Updated user with id: ${req.params.id}`,
                data: result
            })
        }else{
            res.send({ 
                message: 'Not found user with given id'
            })
        }
    }else{
        const resp = {}
        if(!req.params.id){
            resp['error_id'] = 'Kindly enter a valid id'
        }
        if(!req._body){
            resp['error_body'] = 'Kindly provide body with the request'
        }

        res.status(500).send({ 
            ...resp
        })
    }
}
