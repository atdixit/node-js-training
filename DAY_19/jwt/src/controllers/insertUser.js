import { v4 as uuid } from 'uuid'

import { insertUser } from "../services/insertUser.js"
import { generateSecurePassword } from "../utils/securedPassword.js"

const validate = ( str ) => {
    if(str !== null && str !== ''){
        return true
    }
}

export const handleInsertUser = async ( req, res ) => {
    if(req._body && req.body.name && req.body.city && req.body.education && req.body.password ){
        const insert_data = {
            id: uuid(),
            name: req.body.name,
            city: req.body.city,
            education: req.body.education,
            password: generateSecurePassword(req.body.password)
        }
        let flag = true
        for(let i in insert_data){
            if(!validate(i)){
                flag = false
            }
        }
        if(flag){
            const result = await insertUser( insert_data )
            if(result){
                res.status(200).send({
                    message: `Successfully created a user with id: ${result.id}`,
                    data: result
                })
            }else{
                res.status(500).send({
                    error: 'Unable to create the entry, please enter valid info'
                })
            }
        }else{
            res.status(500).send({
                error: 'Unable to create the entry, please enter valid info'
            })            
        }

    }else{
        res.status(500).send({
            error: 'Send a valid body with the request'
        })
    }
}