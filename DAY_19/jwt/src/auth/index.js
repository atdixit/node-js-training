// BASIC AUTH FOR THE API

const { AUTH_USERNAME, AUTH_PASSWORD } = process.env || {}

export const basicAuth = ( req, res, next ) => {
    // console.log('Authorization',req.headers.authorization)
    if(req.headers.authorization){
        const a = 'Basic ' + new Buffer.from(`${AUTH_USERNAME}:${AUTH_PASSWORD}`).toString('base64')
        if(a === req.headers.authorization){
            // console.log('Auth Successful!')
            next()
        }else{
            res.send({ 
                message: 'Auth Failed! Kindly check the username and password.'
            })
        }
    }else{
        res.send({ 
            error: 'You will need a login auth to proceed!'
        })
    }
}