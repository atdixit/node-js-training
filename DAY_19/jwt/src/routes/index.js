import express from 'express'
import passport from 'passport'
import { 
    handleInsertUser, 
    handleGetUser,
    handleGetAllUsers, 
    handleDeleteAllUser, 
    handleDeleteUser, 
    handleUpdateUserById, 
    handleLogin 
} from '../controllers/index.js'
import { passportUsage } from '../utils/passport.js'

passportUsage(passport)
const router = express.Router()

router.get('/get-user/:id',passport.authenticate('jwt',{ session: false }),handleGetUser)
router.get('/get-all-users',passport.authenticate('jwt',{ session: false }), handleGetAllUsers)
router.post('/insert-user',passport.authenticate('jwt',{ session: false }),handleInsertUser)
router.post('/login-user',handleLogin)
router.put('/update-user/:id',passport.authenticate('jwt',{ session: false }), handleUpdateUserById)
router.delete('/delete-user/:id',passport.authenticate('jwt',{ session: false }), handleDeleteUser)
router.delete('/delete-all-users',passport.authenticate('jwt',{ session: false }),handleDeleteAllUser)

export default router