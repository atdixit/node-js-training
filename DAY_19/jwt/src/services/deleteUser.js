import { User } from "../models/User.js"

export const deleteUserById = async ( id ) => {
    const result = await User.destroy({
        where: {
            id
        },
        returning:true,
        restartIdentity: true
    })
    return result
}

export const deleteAllUsers = async () => {
    await User.destroy({
        truncate: true,
        restartIdentity: true
    })
}
