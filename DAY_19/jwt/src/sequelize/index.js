import { Sequelize } from 'sequelize';

const { DB_HOST, DB_USER, DB_NAME, DB_PASS } = process.env 
// || {
//     DB_HOST: 'localhost',
//     DB_USER: 'postgres',
//     DB_NAME: 'usertest',
//     DB_PASS: 'root'
// }

export const sequelize = new Sequelize(DB_NAME,DB_USER,DB_PASS,{
    logging: console.log,
    host: DB_HOST,
    dialect: 'postgres'
})

export const connect = async () => {
    try {
        await sequelize.authenticate()
        console.log('Connection Successful!')
        await sequelize.sync()
        console.log('Database Synced!')
    }catch(error){
        console.log('Unable to connect: ',error)
    }
}

export const syncOnly = async () => {
    try {
        await sequelize.sync()
    }catch(error){
        console.log('Unable to Sync!',error)
    }
}